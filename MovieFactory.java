package com.Week6_Assignment;

public class MovieFactory {

	public Movies getMovies(String movieType) {

		if (movieType == null) {
			return null;
		}
		if (movieType.equalsIgnoreCase("MOVIES_COMMING")) {
			return new MoviesComing();
		}
		else if (movieType.equalsIgnoreCase("MOVIES_IN_THEATRES")) {
			return new MoviesInTheatres();
		} 
		else if (movieType.equalsIgnoreCase("TOP_RATED_INDIA")) {
			return new TopRatedIndia();
		} 
		else if (movieType.equalsIgnoreCase("TOP_RATED_MOVIES")) {
			return new TopRatedMovies();
		}
		return null;
	}

}
