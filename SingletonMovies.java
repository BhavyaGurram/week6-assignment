package com.Week6_Assignment;
import java.sql.*;
public class SingletonMovies {
	private static Connection conn=null;
	private SingletonMovies() {
		
	}
	public static Connection getSingletonMovies() {
		try {
			if(conn==null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn=(Connection)DriverManager.getConnection("jdbc:mysql://localhost:3306/movies?autoReconnect=true&useSSL=false","root","root");
				System.out.println("Database connected successfully\n");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
	
